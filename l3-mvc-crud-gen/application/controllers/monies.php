<?php

class Monies_Controller extends Base_Controller {

public static function testing() {
	echo 'Monies ready!';
}
	
///////////////////////////////////////////////
/**
* properties 
*/

//httpVerb_functionName stuff
public $restful = true;

//use common layout view \views\layouts\base
public $layout = 'layouts.base';

///////////////////////////////////////////////
/**
* api auth
*/
	public function post_authenticate() {
 
	}
///////////////////////////////////////////////
/**
* CRUD - Read
*/
	public function get_all() {
				
		//fetch data from model
		$data = Money::getAll(); 
		
		if($data == false || !(bool)$data)  {
		return Redirect::error('500');
		}
		
		// set title
		$this->layout->titleContent = 'get_all - from monies controller';
		
		//pipe the output to view
		$this->layout->mainContent = View::make('monies.showAll')
								->with('heading','Heading - View all')
								->with('outputs',$data);
	}

///////////////////////////////////////////////
/**
* CRUD - Read - with limit
*/
	public function get_allWithLimit() {
				
		//fetch data from model
		$data = Money::getAllWithLimit(2); 
		
		if($data == false || !(bool)$data)  {
		return Redirect::error('500');
		}
		
		// set title
		$this->layout->titleContent = 'get_allWithLimit - from monies controller';
		
		//pipe the output to view
		$this->layout->mainContent = View::make('monies.showAll')
								->with('heading','Heading - View all with limit')
								->with('outputs',$data);
	}


///////////////////////////////////////////////
/**
* CRUD - Read - by id
*/	
	public function get_by($id) {
		
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
		Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('monies');
		}
		
		//fetch the data from model
		$data = Money::getBy($id);
			
		if ($data == false) {
			Session::flash('flashMsg',Base_Controller::ConstMsgModelEmpty);
			return Redirect::to_route('monies');
		}
		
		//pipe the output to view
		// set title
		$this->layout->titleContent = 'get_by - from Monies controller';
		$this->layout->mainContent = View::make('monies.show')
								->with('heading','Heading - View by id')
								->with('outputs',$data);
	}

////////////////////////////////////////////////////////////
/**
* show for new
*/	
	public function get_new() {
		$this->layout->titleContent = 'get_new - monies controller';
				//pipe forms input to view
		$this->layout->mainContent = View::make('monies.new')
							->with('heading','Heading - Add New - monies controller');
							
	}
////////////////////////////////////////////////////////////
/**
* CRUD - Create - new
*/	
	public function post_new() {
		//validate input
		$validation = getValidationResult(Input::all(),Money::$rules);
		if ($validation->fails()) {
			//return Response::make('test');
			
		//flash error keys using $rules array
		//to get around bug: with_errors not allow dynamic error key enumeration
			Session::flash('errorKeys',Money::$rules);
			
		//redirect to input view, display err msg
			return Redirect::to_route('new-money')
					->with_errors($validation)
					->with_input();
		}//fail
		else {
			//for testing
			//return Response::make('Passed validation. Ready to add new!');
					
			//insert into Model
			$result = Money::postNew(Input::all());
			
			if ($result == false || (bool)$result == false) {
				//failed, send 500
			Session::flash('flashMsg',
			'New money '.Base_Controller::ConstMsgCreateNewFalse);
			return Redirect::to_route('monies');
			}
			else {
				//success, set flash msg, redirect to success page 
			Session::flash('flashMsg',
			'New money '.Base_Controller::ConstMsgCreateNewTrue);
			return Redirect::to_route('monies');
			}
			
						
		}//validation success
		
	}

////////////////////////////////////////////////////////////
/**
* CRUD Read - show for edit/update
*/	
	public function get_edit($id) {
		
		if ($id== '' || is_null($id) || $id <=0) {
		Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
		return Redirect::to_route('monies');
		}
		
		//for testing
		//return Response::make("id = {$id}. Ready to update!");		
		
		//fetch data from model
		$data = Money::getBy($id);
		
		if($data == false ) {
			Session::flash('flashMsg',Base_Controller::ConstMsgModelEmpty); 
			return Redirect::to_route('money',$id);	
		}
		
		 
		//pipe the output to view
		// set title
		$this->layout->titleContent = 'get_edit - from monies controller';
		$this->layout->mainContent = View::make('monies.edit')
								->with('heading','Heading - View by id for edit')
								->with('outputs',$data);
									
	}

////////////////////////////////////////////////////////////
/**
* CRUD Put - update
*/
	public function put_update() {
	//validate input data
	//update db
	//flash msg
	//fail -> redirect -> flsh msg
	
	//grab id from input, check it
	$id = Input::get('id');
	
	if ($id == '' || is_null($id) || $id <=0) {
	Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
		return Redirect::to_route('monies');
	}
		
	//validate input data
	$validation = getValidationResult(Input::all(),Money::$rules);
	if($validation->fails()) {
		//flash error keys using $rules array
		//to get around bug: with_errors not allow dynamic error key enumeration
		Session::flash('errorKeys',Money::$rules);
				
		//redirect back to edit w/ errors n input
		return Redirect::to_route('edit-money',$id)
				->with_errors($validation)
				->with_input();		
	}
	else {
	
	//process result
	//testing
	//return Response::make("id = {$id}. Ready to update!");	
	  
		// if update fails, send 500. Else, redirect to author page
		$result = Money::putUpdate($id,Input::all());
		
		if ($result <=0) {
		Session::flash('flashMsg','Money '.Base_Controller::ConstMsgUpdateFalse);
			return Redirect::to_route('money',$id);
		}
		else {
		Session::flash('flashMsg','Money '.Base_Controller::ConstMsgUpdateTrue);
			return Redirect::to_route('money',$id);
		}		
	}//input validated
   }//put_update

////////////////////////////////////////////////////////////
/**
* CRUD - delete
*/
	public function delete_remove() {
		//get id, check
		$id = Input::get('id');
		
		if ($id == '' || is_null($id) || $id <=0) {
			Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('monies');
		}
		
		//for testing
		//return Response::make('delete success!');
			
		//find id, then delete
		if (Money::deleteRemove($id) <=0) {
			Session::flash('flashMsg','Money '.Base_Controller::ConstMsgDeleteFalse);
			return Redirect::to_route('money',$id);
		}
		
		else {
			Session::flash('flashMsg','Money '.Base_Controller::ConstMsgDeleteTrue);
			return Redirect::to_route('monies');
		}
		
	}

///////////////////////////////////////////////	
/**
* get with joins 1
*/
	public function get_test2innerjoins() {
		//test
		$id = 2;
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
		Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('monies');
		}
			
		// get data
		$data = Money::getTwoInnerJoins($id);
		if (!(bool)$data) {
			return Response::make((bool)$data);
		}
		else {
			$this->layout->titleContent = 'get_test2innerjoins';
			$this->layout->mainContent = View::make('monies.test2joins')
										->with('heading','getModelRecordWithTwoJoins')											->with('outputs',$data);
		}	
	
	 }		

////////////////////////////////////////////////////////////
/**
* //get with joins 2
*/
	public function get_test3innerjoins() {
		//test
		$id = 1;
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
			Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('monies');
		}
		
		//get data
		$data = Money::getThreeInnerJoins($id);
		
		if(!(bool)$data) {
			return Response::make((bool)$data);
		}
		else {
			$this->layout->titleContent = 'get_test3innerjoins';
			$this->layout->mainContent = View::make('monies.test3joins')
								->with('heading','getModelRecordWithThreeInnerJoins')
								->with('outputs',$data);	
		}
		
	}
	
////////////////////////////////////////////////////////////
				//json//
////////////////////////////////////////////////////////////
/**
* GET all - json
*/
	public function get_jsonMonies() {
		Money::$json = true;
		$data = Money::getAll();
		
		if ($data == false || !(bool)$data) {
			return Response::json(array('error'=> '404',
							 			'description'=> Base_Controller::ConstMsgModelEmpty	
										)
								,500);
		}
		else {
			return Response::eloquent($data);
		}
	}
////////////////////////////////////////////////////////////
/**
* GET by id - json
*/
	public function get_jsonMoneyBy($id) {
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
		    return Response::json(array
							('error'=>'404',
							 'description' => Base_Controller::ConstMsgIdInvalid		
							),500);
		}
		
		//fetch the data from model
		Money::$json = true;
		$data = Money::getBy($id);
		if ($data == false) {
			return Response::json(array
							('error'=>'404',
							 'description' => Base_Controller::ConstMsgModelEmpty		
							),500);
		}
		return Response::eloquent($data);
	}	
}//class

?>
