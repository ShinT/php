<html>
	<h1>{{$heading}}</h1>

{{-- section to render any flash error msg after submit new --}}
{{render('shared.validationErrors') }}

{{-- use form class to build forms --}}
{{Form::open('monies/create','POST') }}

{{-- csrf security check --}}
{{Form::token()}}

<p>

{{Form::label('#field1','#field1:') }} <br/>
{{Form::text('money_#field1',Input::old('money_#field1')) }} <br/>
{{-- Specify Input::old('dbField')) so no need to retype after submit w/ validate err --}}
	
</p>

<p>
{{Form::label('#field2','#Field2:') }} <br/>
{{Form::textarea('money_#field2', Input::old('money_#field2')) }} <br/>
{{-- Specify Input::old('dbField')) so no need to retype after submit w/ validate err --}}
</p>

<p> {{Form::submit('Add money') }} </p>

{{Form::close() }}

</html>


