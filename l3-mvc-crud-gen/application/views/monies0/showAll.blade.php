<h1> {{$heading}}- monies/showAll.blade</h1>
{{-- var from $this->layout->mainContent ->with('heading','stuff') --}}	
	
<h5>List of monies from money model</h5>
<ol>

{{--10. CRUD - Read - List all monies - in index.blade view as links, then link each other to named route--> --}}

{{-- pass a 3rd param: array($money->id) b/c route 'money/(:any)' 'monies@view' requires money id to load data --}}

{{-- access output fields thru facade, so change in data store wont affect view. Just need to make changes in model --}}

@foreach($outputs as $output)
	<li>Money name: {{HTML::link_to_route('money', $output->money_#field1, array($output->id)) }} 
	</li>
{{--link to named route 'money/(:any)', array('as'=> 'monies',
	 'uses'=> 'monies@view'))--}}
@endforeach	
</ol>	
	
{{-- link to add new money --}}
<p>{{HTML::link_to_route('new-money','Link to Add New money page') }} </p>
