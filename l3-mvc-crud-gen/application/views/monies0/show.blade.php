{{-- CRUD - Read - by id - create monies.view view file /views/monies/view.blade.php to view specific money by id --}}

<h1> {{ $heading }} - monies/show.blade</h1>

{{-- escape any output data displayed as html in views to prevent xss, stop script injection w/ HTML::entities or e --}}
<h2> Money's Field1 - escaped by HTML::entities: {{HTML::entities($outputs->money_#field1)}} </h2>
<h2> Money's Field1:- escaped by e: {{e($outputs->money_#field1)}} </h2>

<p>Money's field2: {{$outputs->money_#field2 }}</p>

<p><small> Updated at: {{$outputs->money_updated_at }} </small> </p>


<span>
{{-- link to a list of monies page to go back to Home page --}}
	{{HTML::link_to_route('monies','Show all monies') }} <br/><br/>

{{-- Option to edit an money here n will lead to edit.blade for editing --}}
{{--Also pass a 3rd param: array($money->id) b/c route /(:any) requires money id to load data. $money-id goes to edit.blade to be stored as hidden field for submit event to update--}}

{{-- CRUD - edit money, get from money/$id/edit action --}}
	{{HTML::link_to_route('edit-money','Edit',array($outputs->id)) }} <br/><br/>

{{--  CRUD - delete money, form submit to money/delete action --}}
	{{Form::open('money/delete','DELETE',array('style'=>'display:inline;')) }}
	
	{{-- set csrf token for security check --}}
	{{Form::token()}}
	
{{-- set hidden field to store $id to send to submit for delete --}}
	{{Form::hidden('id', $outputs->id) }}
	
	{{Form::submit('Delete') }}

	{{Form::close() }}

{{-- set route --}}
</span>	


