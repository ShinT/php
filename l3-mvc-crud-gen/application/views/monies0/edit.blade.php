<h1> {{"$heading $outputs->money_#field1"}}  </h1>

{{-- section to render any flash error msg after submit--}}
	{{ render('shared.validationErrors') }}


{{-- create a PUT form to update/edit an money on validation success --}}
{{ Form::open('money/update','PUT') }}

{{-- csrf security check--}}
{{Form::token()}}

<p>
	{{Form::label('#field1','#Field1:')  }} <br/>
	{{Form::text('money_#field1',$outputs->money_#field1) }} <br/>
</p>
	
<p>
	{{Form::label('#field2','#Field1:')  }} <br/>
	{{Form::textarea('money_#field2',$outputs->money_#field2) }} <br/>
</p>

	
{{--hidden element to store id. Later to be used for form submit for db update--}}
	{{--note: will only be posted on post and put. On get, input::get('id') returns null--}}
	{{Form::hidden('id',$outputs->id)  }}

<p>	
	{{Form::submit('Update') }}
</p>	
{{Form::close() }}	

{{-- Next, create a link in the /views/view.blade, where an money is displayed after being clicked. That way, an option to edit is enabld there n will lead to this page for editing--}}

{{-- Create a PUT route for update & a controller action to process update	--}}
