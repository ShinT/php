<?php
include ('\..\..\..\..\.\php\utils\fileIO.php');


////////////////////////////////////////////////////////////
/**
* menu options
*/
if (!isset($argv[1]) || $argv[1] == 'help' || $argv[1] == '?' ) {
	echo "=================================================\n";
	echo "Auto generate tasks for Laravel 3\n";
	echo "=================================================\n";
	echo "\n";
	echo "Specify a task. Then supply the param(s) to run it.\n";       
	echo "\n";
	echo "=================================================\n";       
	echo "task keyword - desc - param 1 - param 2 - param 3\n";
	echo "=================================================\n";
	echo "paths - check file paths - Param:none \n";
	echo "--------------------------------------------------\n";       
	echo "init - run first time setup - Param:none \n";
	echo "--------------------------------------------------\n";
	echo "gencontroller - generate controller - Param1:singularName - Param2:pluralName\n";
	echo "--------------------------------------------------\n";
	echo "genmodel - generate model - Param1:singularName - Param2:pluralName\n";
	echo "--------------------------------------------------\n";
	echo "genview - generate view - Param1:singularName - Param2:pluralName\n";
	echo "--------------------------------------------------\n";
	echo "genmvc - generate all mvc - Param1:singularName - Param2:pluralName\n";
	echo "--------------------------------------------------\n";
	echo "gentable - create db table - Param:pluralName\n";
	echo "--------------------------------------------------\n";
	echo "gentabletestdata - gen test data - Param:tableName\n";
	echo "--------------------------------------------------\n";
echo "gentest - generate test file Param1:filename - Param2:model|view|controller\n";
	echo "To generate view, specify view subfolder for Param1. i.e viewSubfolder/filename\n";
	echo "--------------------------------------------------\n";
	
	
echo "\nEnter a task to use l3gen. Choose task: 'help' or '?' for details.\n";	
	return;
}

/////////////////////////////////////////////////////////////
/**
* file paths consts
*/
define('Path_l3gen',dirname(__FILE__));

define('Path_tasks',dirname(Path_l3gen));

define('Path_tmp',Path_l3gen .'/tmp');

define('Path_sourceController', Path_l3gen .'/tmp/controllers');

define('Path_sourceModel',Path_l3gen .'/tmp/models');

define('Path_sourceView',Path_l3gen .'/tmp/views');

define('Path_application', dirname(Path_tasks));

define('Path_targetController',Path_application .'/controllers');

define ('Path_targetModel', Path_application .'/models');

define('Path_targetView',Path_application .'/views');

/////////////////////////////////////////////////////////////
/**
*entry points
*/

if ($argv[1] == 'paths') {
 	echo 'l3gen: '. Path_l3gen."\n\n";
 	echo 'tmp: '. Path_tmp."\n\n";
 	echo 'sourceController: '. Path_sourceController."\n\n";
 	echo 'sourceModel: '. Path_sourceModel."\n\n";
	echo 'sourceView: '. Path_sourceView."\n\n";
	echo 'tasks: '. Path_tasks."\n\n";
	echo 'application: '. Path_application."\n\n";
	echo 'targetController: '. Path_targetController."\n\n";
	echo 'targetModel: '. Path_targetModel."\n\n";
	echo 'targetView: '. Path_targetView."\n\n";
}


if ($argv[1] == 'init') {
	setInitProject();
	
}

if ($argv[1]== 'gencontroller') {
	 setGenController($argv[2],$argv[3]);
}

if ($argv[1] == 'genmodel') {
	setGenModel($argv[2],$argv[3]);
	
}

if ($argv[1]== 'genview') {
	setGenView($argv[2], $argv[3]);
	
}

if ($argv[1] == 'genmvc') {
	setGenController($argv[2],$argv[3]);
	setGenModel($argv[2],$argv[3]);
	setGenView($argv[2],$argv[3]);
}

if ($argv[1] == 'gentable') {
echo "-cd to framework to run generate.php\n";	
echo "-Run php artisan generate migration: create_x_table id:integer, something:string\n";
echo "-check file in \migration. Bug: have to remove the extra auto-id column\n";
echo "note: cmd will auto-add timestamp() columns\n";
echo "note: column options: columnX:string:unique columnY:integer:nullable \n";
echo "-Run php artisan migrate\n";
}

if ($argv[1] == 'gentabletestdata') {
echo "- cd to framework to run generate.php\n";
echo "- Run php artisan generate migration: add_x \n";
echo "- remove the useless code\n";
echo "- update():\n";
echo "- DB::table('tableName')->insert(array('column1'=> 'data1', created_at=> date(('Y-m-d H:m:s'))) \n";
echo "down():\n";
echo "DB::table('tableName')->where('columnx', '=', 'val1')->delete() \n";	
}

if ($argv[1] == 'gentest') {
	setGenTest($argv[2],$argv[3]);
}


return;
/////////////////////////////////////////////////////////////
/**
* run first time setup for laravel 3 project 
*/
	function setInitProject() {
		setBaseController();
		setSharedModel();
		setBaseViews();
		setRoutes();
		setRoutesInclude();
		
	}
/////////////////////////////////////////////////////////////
/**
* set /models/sharedModel
*/
	function setSharedModel() {
		if (setCopy(Path_sourceModel .'/sharedModel.php',
		Path_targetModel .'/sharedModel.php')== true) {
			echo "create sharedModel - done!\n";
		}
		else {
			echo "cannot create sharedModel - not done!\n";
		}
	
	}
/////////////////////////////////////////////////////////////
/**
* set /controllers/base
*/
	function setBaseController() {
		if (setFile(Path_sourceController. '/2base.php',
		Path_targetController. '/base.php','a') == 1)
		{			
			echo "set base controller - done!\n";	
		}
		else {
			echo "cannot set base controller - not done!\n";
		}
	}
/////////////////////////////////////////////////////////////
/**
* set /views stuff
*/
	function setBaseViews() {
		// set layout folder
		if (setDirectory(Path_targetView,'layouts')== true) {
			echo "set up /views/layouts folder - done!\n";
			
			// set layout files
			setViewLayoutFiles();
		}
		else {
			echo "cannot set /views/layouts folder - not done!\n";
		}
		
		// set shared folder
		if (setDirectory(Path_targetView,'shared')== true) {
			echo "set /view/shared folder -done!\n";
			
			//set shared files
			setViewSharedFiles();
		}
		else {
			echo "cannot set /views/shared folder -not done!\n";
		}
		
	}	
/////////////////////////////////////////////////////////////
/**
* set views/shared stuff
*/
	function setViewSharedFiles() {
		//setCopy(Path_source
		if (getIsDirectory(Path_sourceView . '/shared')) {
			//echo "/shared exists!\n";
			
			//set files to /shared
			if(	setCopy(Path_sourceView .'/shared/validationErrors.blade.php',
			Path_targetView .'/shared/validationErrors.blade.php') == true) {
				echo "set /views/shared/validationErrors.blade - done!\n";
			}
			else {
				echo "cannot set /views/shared/validationErrors.blade - not done!\n";
			}
			
		}
		else {
			echo "/shared folder not found - not done!\n";	
		}
	} 
/////////////////////////////////////////////////////////////	
/**
* set /views/layouts stuff
*/	
	function setViewLayoutFiles() {
		if (getIsDirectory(Path_sourceView . '/layouts')) {
			//echo "/layouts exists\n";
			
			//set files to /layouts
			if (setCopy(Path_sourceView .'/layouts/base.blade.php',
			Path_targetView .'/layouts/base.blade.php')== true) {
				echo "set /views/layouts/base.blade - done!\n";
			}
			else {
				echo "cannot set /views/layouts/base.blade - not done!\n";
			}
		}
		else {
			echo "/layouts folder not found - not done!\n";
		}
	}

/**
* generate new view 
*/
	function setGenView($viewNameInSingular,$viewNameInPlural) {
		//sanitize
		$viewNameInSingular = strtolower($viewNameInSingular);
		$viewNameInPlural = strtolower($viewNameInPlural);
		
		//check
		if (getIsDirectory(Path_targetView ."/{$viewNameInPlural}")) {
		echo "{$viewNameInPlural} view not created. Already exists - not done!\n";
		return;
		}
		
		//check source /objects dir exist
		if (getIsDirectory(Path_sourceView . '/objects')) {
			if (getFileCount(Path_sourceView . '/objects')>0) {
				//create new sub dir in target
				if (setDirectory(Path_targetView,$viewNameInPlural) == true) {
					echo "Generate view directory - done!\n";
					
					setGenViewFiles($viewNameInSingular,$viewNameInPlural);		
				}
			}
			else {
				echo "Generate view - file not found - not done!\n";
			}
		}
		else {
			echo "Generate view - directory not found - not done!\n";
		}
	}
	
/**
* generate view files
* @param string $viewNameInSingular
* @param string $viewNameInPlural
* 
*/	
	function setGenViewFiles($viewNameInSingular,$viewNameInPlural) {
		$files = getFiles(Path_sourceView .'/objects');
		foreach($files as $file) {
			//get contents from file
			$contents = getFileContents(Path_sourceView ."/objects/{$file}");
			
			//search n replace
			$contents = setSearchAndReplaceSingularPlural(
			$contents,'object','Objects',$viewNameInSingular,$viewNameInPlural,
			'#');
			 
			//write to target
			$fileNameParts = explode('.',$file); // get name
			if (setWrite(Path_targetView .
			"/{$viewNameInPlural}/{$fileNameParts[0]}.blade.php" ,$contents)> 0) { 
				echo "generate {$fileNameParts[0]}.blade - done!\n";
			}
			else {
				echo "cannot generate {$fileNameParts[0]}.blade - not done!\n";
			}
	 	}
	}
	
	function setTest2($viewNameInSingular,$viewNameInPlural) {
		
		foreach($files = getFiles(Path_sourceView .'/objects') as $file) {
			//get contents from file
			$contents = getFileContents(Path_sourceView ."/objects/{$file}");
			
			//search n replace
			$contents = setSearchAndReplaceSingularPlural(
			$contents,'object','Objects',$viewNameInSingular,$viewNameInPlural,
			'#');
			
			echo $contents . "\n";
			return; 
			//write to target
			/*$fileName = explode('.',$file); // get name
			if (setWrite(Path_targetView .
			"/{$viewNameInPlural}/{$fileName}.blade.php" ,$contents)> 0) { 
				echo "generate {$fileName}.blade - done!\n";
			}
			else {
				echo "cannot generate {$fileName}.blade - not done!\n";
			}*/
	 	}
	}
	
/////////////////////////////////////////////////////////////
/**
* replace routes file w/ one that has a custom include 
*/	
	function setRoutes() {
		if (setCopy(Path_tmp .'/routes.php',
		Path_application .'/routes.php') == true) {
			echo "routes file replaced - done!\n";
		}
		else {
			echo "cannot replace routes file - not done!\n";
		}
	}	

/////////////////////////////////////////////////////////////
/** routes
* set up includeRoutes for dynamic custom routes
*/	
	function setRoutesInclude() {
		if (setCopy(Path_tmp .'/includeRoutes.php', 
		Path_application . '/includeRoutes.php')== true) {
			echo "set includeRoutes file - done!\n";
		}
		else {
			echo "cannot set includeRoutes file - not done!\n";
		}		
	}

/**
* update controller routes 
*/	
	function setUpdateRoutes($controllerNameInSingular, $controllerNameInPlural) {
		//sanitize
		$controllerNameInSingular = strtolower($controllerNameInSingular);
		$controllerNameInPlural = strtolower($controllerNameInPlural);
		
		//read snips
		$contents = getFileContents(Path_tmp .'/includeRoutes.snips.php');
		
		if ($contents != '') {
			$contents = setSearchAndReplaceSingularPlural($contents,'Object'
			, 'Objects',$controllerNameInSingular,$controllerNameInPlural,'#');
			
			//append
			if (setAppend(Path_application .'/includeRoutes.php', 
			$contents) >0) {
				echo "Routes update - done!\n";
			}
			else {
				echo "Cannot update routes - not done!\n";
			}			
		} 
		else {
			echo "No snips found. Cannot update routes - not done!\n";
		}
	}
	
/////////////////////////////////////////////////////////////
/** Controllers
* generate new controller
*/
	function setGenController($nameInSingular, $nameInPlural) {
		// sanitize names
		$nameInSingular = strtolower($nameInSingular);
		$nameInPlural = strtolower($nameInPlural);
		
		if (file_exists(Path_targetController ."/{$nameInPlural}.php")) {
		echo "Controller {$nameInPlural} not created. Already exists - not done!\n";			}
						
		// get contents
		$contents = getFileContents(Path_sourceController .'/objects.tmp.php');
		
		//search n replace
		$contents = setSearchAndReplaceSingularPlural($contents,
					'OBJECT', 'oBjEcTs',$nameInSingular, $nameInPlural,'#');
		//echo $contents;
		
		//write to target
		if (setWrite(Path_targetController .'/'.$nameInPlural.'.php',$contents) > 0) {
			echo "{$nameInPlural} controller generated - done!\n";
			
			//update routes
			setUpdateRoutes($nameInSingular,$nameInPlural);
		}
		else {
			echo "Cannot generate controller - not done!\n";
		}
	}
	
	
	
/////////////////////////////////////////////////////////////	
/**
* model 
*/	

	function setGenModel($nameInSingular, $nameInPlural) {
				
		//sanitize
		$nameInSingular = strtolower($nameInSingular);
		$nameInPlural = strtolower($nameInPlural);
		
		// check exist
		if (file_exists(Path_targetModel ."/{$nameInSingular}.php")) {
		echo "Model {$nameInSingular} not created. Already exists - not done!\n";			return;
		}
			
		//get content
		$contents = getFileContents(Path_sourceModel .'/object.tmp.php');
		//echo $contents;
				
		//search replace
		$contents = setSearchAndReplaceSingularPlural($contents,'Object','Objects',
		$nameInSingular,$nameInPlural,'#');
		//echo $contents;
			
		//write
		if (setWrite(Path_targetModel ."/{$nameInSingular}.php",$contents)>0) {
			echo "{$nameInSingular} model generated - done!\n";
		}
		else {
			echo "Cannot generate model - not done!\n";
		}
	}	


/////////////////////////////////////////////////////////////	
/**
* generate test. $module: model|view|controller $name i.e $name.test.php 
* @param string $module
* @param string $name
*/
	function setGenTest($name,$module) {
		//tell l3gen the path to create this file at
		
		//get contents
		$contents = FileIO::getFileContents(Path_tmp .'/test.tmp.php');
		
		//search n replace
		$contents = FileIO::setSearchAndReplace($contents,'object', $name,'#');
		$path;		
		
		if ($module == 'model') {
		$path = Path_targetModel;				
		}
		
		if ($module == 'view') {
		$path = Path_targetView ;				
		}
		
		if ($module == 'controller') {
		$path = Path_targetController;				
		}
		
		
		//write to target
		if ($path != '') {
			if (FileIO::setWrite("{$path}/{$name}.test.php",$contents)> 0 ) {
			 	echo "{$path}/{$name}.test.php test file generated - done!\n";
			 }
			 else {
			 	echo "Cannot generate test file - not done!\n";
			 }	
			//echo "ready to generate: {$path}/{$name}.test.php"; 
		}
		else {
			echo "'{$module}' path not found. Cannot generate test file - not done!\n";
		}
			
	}	


	
/////////////////////////////////////////////////////////////
// 						FileIO stuff
/////////////////////////////////////////////////////////////

/**
* opens file and reads contents
* @param string $filePath
* @return string
*/
function getFileContents($filePath) {
	return @file_get_contents($filePath);
}


/**
* returns a list of file names in directory
* @param string $path
* @return array
*/
	function getFiles($path) {
		$items = @scandir($path);
			if (!empty($items)) {
				foreach($items as $item) {
					if (!getIsDirectory("{$path}/{$item}")) {
					$sanitizedItems[] = $item;
					}	
				}	
			}
		
		if (!isset($sanitizedItems)) {
			$sanitizedItems = false;
		}
		return $sanitizedItems;	
	}
	
/**
* returns a list of file & directory names in directory
* @param string $path
* @param array
*/	
	function getFilesAndDirectories($path) {
		//$path = Path_targetModel . '/1testfolder';
		$items = @scandir($path);
		if (!empty($items)) {
			foreach($items as $item) {
				if ($item != '.' && $item != '..') {
					$sanitizedItems[] = $item;
				}
			}	
		}
				
		if (!isset($sanitizedItems)) {
			$sanitizedItems = false;
		}	
		return $sanitizedItems;
	}

/////////////////////////
/**
* returns file count in $path directory
* @param string $pathName
* @return int
*/
	function getFileCount($path) {
		return count(glob("{$path}/*.*"));
	}

/**
* returns file & directory count in $path directory
* @param string $path
* @return int
*/
	function getFileAndDirectoryCount($path) {
		return count(@scandir($path));	
	}

/**
* check whether path is a directory
* @param string $path
* @return bool
*/	
	function getIsDirectory($path) {
		return is_dir($path);
	}


/**
* copy contents from $fileSource, find search terms and replace. Supports singular/plural term changes. Returns processed contents. 
* @param string $contents
* @param string $searchTermInSingular
* @param string $searchTermInPlural
* @param string $replaceTermInSingular
* @param string $replaceTermInPlural
* @param string $delimiter
* 
*/
	function setSearchAndReplaceSingularPlural($contents, $searchTermInSingular
				, $searchTermInPlural,$replaceTermInSingular,$replaceTermInPlural,
						$delimiter) {
	
	//sanitize input
	$replaceTermInSingular = strtolower($replaceTermInSingular);
	$replaceTermInPlural = strtolower($replaceTermInPlural);
	$searchTermInSingular = strtolower($searchTermInSingular);
	$searchTermInPlural = strtolower($searchTermInPlural);
	
	$contents = str_replace($delimiter.ucfirst($searchTermInSingular).$delimiter
	,ucfirst($replaceTermInSingular),$contents);
	
	$contents = str_replace("{$delimiter}{$searchTermInSingular}{$delimiter}",
	$replaceTermInSingular,$contents);
	
	$contents = str_replace($delimiter.ucfirst($searchTermInPlural).$delimiter
	,ucfirst($replaceTermInPlural),$contents);
	
	$contents = str_replace("{$delimiter}{$searchTermInPlural}{$delimiter}"
	,$replaceTermInPlural,$contents);
		
	return $contents;
	}

/**
* copy contents from $fileSource, find $replaceTarget item, replace item w/ $replaceSource. Then, write to $fileTarget
* @param string $fileSource
* @param string $fileTarget
* @param string $replaceSource
* @param string $replaceTarget
* @return int 0|1
*/
	function setSearchAndReplace($contents, $searchTerm, $replaceTerm, $delimiter) {
	
	//sanitize
	//$searchTerm = strtolower($searchTerm);
	
	//search n replace
	$contents = str_replace("{$delimiter}{$searchTerm}{$delimiter}"
	,$replaceTerm,$contents);
	
	return $contents;
	
	}
///////////////////////////
/**
* write $contents to $fileNameWithPath. Supports new file creation.
* @param string $fileNameWithPath
* @param string $contents
* @return int 0|1
*/	function setWrite($fileNameWithPath,$contents) {
		
		$fStream = fopen($fileNameWithPath,'w');
		//fopen(Path_sourceController. '/testsource.php','w');
		
		/*$contents = "<?php\n";
		$contents = $contents."function testing() {\n";
		$contents = $contents."echo 'testing write contents';\n";
		$contents = $contents."?>\n";*/
		
		$result = fwrite($fStream,$contents."\n");
		fclose($fStream);
		return $result;
		
	}		
///////////////////////////
/**
* appends text to existing file
* @param string $file
* @param string $contents
* @return int 0|1
*/	
	function setAppend($file,$contents) {
		$fStream = fopen($file,'a');
		$result = fwrite($fStream,$contents."\n");
		fclose($fStream);
		return $result;
		
	}

///////////////////////////
/**
* copy file from $sourcePath to $targetPath. Override if exists
* @param string $sourcePath
* @param string $targetPath
* @return bool true
*/
	function setCopy($sourcePath, $targetPath) {
		return @copy($sourcePath, $targetPath);
	}
	
///////////////////////////
/**
* creates a new $directoryName folder/directory given the $path
* @param string $path
* @param string $directoryName
* @return bool true
*/	
	//
	function setDirectory($path, $directoryName) {
		return @mkdir("{$path}/{$directoryName}");
	}

/**
* deletes a file. Returns true if success, null otherwise.
* @param string $fileNameWithPath
* @return bool
*/
	function setDeleteFile($fileNameWithPath) {
		//$fileNameWithPath = Path_targetModel .'/test.txt';
		return @unlink($fileNameWithPath);
		
	}

/**
* deletes the directory, including any files, subdirectories and their files. Returns true if success, null otherwise.
* @param string $directory
* @return bool
*/	
	function setDeleteDirectory($directory) {
		$items = getFilesAndDirectories($directory);
		if ((bool)$items) {
			// gotta check for items in subdir
				foreach($items as $item) {
					if (getIsDirectory("{$directory}/$item")) {
					//echo "folder to delete: {$directory}/$item\n";
					setDeleteDirectory("{$directory}/$item");
					}
					else {
						//delete file
						//echo "file to delete: {$directory}/{$item}\n";	
						setDeleteFile("{$directory}/{$item}");
					}
			
				}
		}
		//echo "finally delete this empty folder: {$directory}/\n";
		return @rmdir("{$directory}/");	
		//echo 'total: '. getFileAndDirectoryCount($directory)."\n";
		
	}

	function setTestDeleteDirectory() {
		$directory = Path_targetModel .'/New folder';
		echo setDeleteDirectory($directory);
	}
	
	
	
	
	
/*exec('dir Path_targetcontrollers',$results);
foreach($results as $result)
echo $result;*/

?>