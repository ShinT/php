<?php

class #Objects#_Controller extends Base_Controller {
	
///////////////////////////////////////////////
/**
* properties 
*/

//httpVerb_functionName stuff
public $restful = true;

//use common layout view \views\layouts\base
public $layout = 'layouts.base';

///////////////////////////////////////////////
/**
* api auth
*/
	public function post_authenticate() {
 
	}
///////////////////////////////////////////////
/**
* CRUD - Read
*/
	public function get_all() {
				
		//fetch data from model
		$data = #Object#::getAll(); 
		
		if($data == false || !(bool)$data)  {
		return Redirect::error('500');
		}
		
		// set title
		$this->layout->titleContent = 'get_all - from #objects# controller';
		
		//pipe the output to view
		$this->layout->mainContent = View::make('#objects#.showAll')
								->with('heading','Heading - View all')
								->with('outputs',$data);
	}

///////////////////////////////////////////////
/**
* CRUD - Read - with limit
*/
	public function get_allWithLimit() {
				
		//fetch data from model
		$data = #Object#::getAllWithLimit(2); 
		
		if($data == false || !(bool)$data)  {
		return Redirect::error('500');
		}
		
		// set title
		$this->layout->titleContent = 'get_allWithLimit - from #Object#s controller';
		
		//pipe the output to view
		$this->layout->mainContent = View::make('#objects#.showAll')
								->with('heading','Heading - View all with limit')
								->with('outputs',$data);
	}


///////////////////////////////////////////////
/**
* CRUD - Read - by id
*/	
	public function get_by($id) {
		
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
		Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('#objects#');
		}
		
		//fetch the data from model
		$data = #Object#::getBy($id);
			
		if ($data == false) {
			Session::flash('flashMsg',Base_Controller::ConstMsgModelEmpty);
			return Redirect::to_route('#objects#');
		}
		
		//pipe the output to view
		// set title
		$this->layout->titleContent = 'get_by - from #Objects# controller';
		$this->layout->mainContent = View::make('#objects#.show')
								->with('heading','Heading - View by id')
								->with('outputs',$data);
	}

////////////////////////////////////////////////////////////
/**
* show for new
*/	
	public function get_new() {
		$this->layout->titleContent = 'get_new - #objects# controller';
				//pipe forms input to view
		$this->layout->mainContent = View::make('#objects#.new')
							->with('heading','Heading - Add New - #objects# controller');
							
	}
////////////////////////////////////////////////////////////
/**
* CRUD - Create - new
*/	
	public function post_new() {
		//validate input
		$validation = getValidationResult(Input::all(),#Object#::$rules);
		if ($validation->fails()) {
			//return Response::make('test');
			
		//flash error keys using $rules array
		//to get around bug: with_errors not allow dynamic error key enumeration
			Session::flash('errorKeys',#Object#::$rules);
			
		//redirect to input view, display err msg
			return Redirect::to_route('new-#object#')
					->with_errors($validation)
					->with_input();
		}//fail
		else {
			//for testing
			//return Response::make('Passed validation. Ready to add new!');
					
			//insert into Model
			$result = #Object#::postNew(Input::all());
			
			if ($result == false || (bool)$result == false) {
				//failed, send 500
			Session::flash('flashMsg',
			'New #object# '.Base_Controller::ConstMsgCreateNewFalse);
			return Redirect::to_route('#objects#');
			}
			else {
				//success, set flash msg, redirect to success page 
			Session::flash('flashMsg',
			'New #object# '.Base_Controller::ConstMsgCreateNewTrue);
			return Redirect::to_route('#objects#');
			}
			
						
		}//validation success
		
	}

////////////////////////////////////////////////////////////
/**
* CRUD Read - show for edit/update
*/	
	public function get_edit($id) {
		
		if ($id== '' || is_null($id) || $id <=0) {
		Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
		return Redirect::to_route('#objects#');
		}
		
		//for testing
		//return Response::make("id = {$id}. Ready to update!");		
		
		//fetch data from model
		$data = #Object#::getBy($id);
		
		if($data == false ) {
			Session::flash('flashMsg',Base_Controller::ConstMsgModelEmpty); 
			return Redirect::to_route('#object#',$id);	
		}
		
		 
		//pipe the output to view
		// set title
		$this->layout->titleContent = 'get_edit - from #objects# controller';
		$this->layout->mainContent = View::make('#objects#.edit')
								->with('heading','Heading - View by id for edit')
								->with('outputs',$data);
									
	}

////////////////////////////////////////////////////////////
/**
* CRUD Put - update
*/
	public function put_update() {
	//validate input data
	//update db
	//flash msg
	//fail -> redirect -> flsh msg
	
	//grab id from input, check it
	$id = Input::get('id');
	
	if ($id == '' || is_null($id) || $id <=0) {
	Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
		return Redirect::to_route('#objects#');
	}
		
	//validate input data
	$validation = getValidationResult(Input::all(),#Object#::$rules);
	if($validation->fails()) {
		//flash error keys using $rules array
		//to get around bug: with_errors not allow dynamic error key enumeration
		Session::flash('errorKeys',#Object#::$rules);
				
		//redirect back to edit w/ errors n input
		return Redirect::to_route('edit-#object#',$id)
				->with_errors($validation)
				->with_input();		
	}
	else {
	
	//process result
	//testing
	//return Response::make("id = {$id}. Ready to update!");	
	  
		// if update fails, send 500. Else, redirect to author page
		$result = #Object#::putUpdate($id,Input::all());
		
		if ($result <=0) {
		Session::flash('flashMsg','#Object# '.Base_Controller::ConstMsgUpdateFalse);
			return Redirect::to_route('#object#',$id);
		}
		else {
		Session::flash('flashMsg','#Object# '.Base_Controller::ConstMsgUpdateTrue);
			return Redirect::to_route('#object#',$id);
		}		
	}//input validated
   }//put_update

////////////////////////////////////////////////////////////
/**
* CRUD - delete
*/
	public function delete_remove() {
		//get id, check
		$id = Input::get('id');
		
		if ($id == '' || is_null($id) || $id <=0) {
			Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('#objects#');
		}
		
		//for testing
		//return Response::make('delete success!');
			
		//find id, then delete
		if (#Object#::deleteRemove($id) <=0) {
			Session::flash('flashMsg','#Object# '.Base_Controller::ConstMsgDeleteFalse);
			return Redirect::to_route('#object#',$id);
		}
		
		else {
			Session::flash('flashMsg','#Object# '.Base_Controller::ConstMsgDeleteTrue);
			return Redirect::to_route('#objects#');
		}
		
	}

///////////////////////////////////////////////	
/**
* get with joins 1
*/
	public function get_test2innerjoins() {
		//test
		$id = 2;
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
		Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('#objects#');
		}
			
		// get data
		$data = #Object#::getTwoInnerJoins($id);
		if (!(bool)$data) {
			return Response::make((bool)$data);
		}
		else {
			$this->layout->titleContent = 'get_test2innerjoins';
			$this->layout->mainContent = View::make('#objects#.test2joins')
										->with('heading','getModelRecordWithTwoJoins')											->with('outputs',$data);
		}	
	
	 }		

////////////////////////////////////////////////////////////
/**
* //get with joins 2
*/
	public function get_test3innerjoins() {
		//test
		$id = 1;
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
			Session::flash('flashMsg',Base_Controller::ConstMsgIdInvalid);
			return Redirect::to_route('#objects#');
		}
		
		//get data
		$data = #Object#::getThreeInnerJoins($id);
		
		if(!(bool)$data) {
			return Response::make((bool)$data);
		}
		else {
			$this->layout->titleContent = 'get_test3innerjoins';
			$this->layout->mainContent = View::make('#objects#.test3joins')
								->with('heading','getModelRecordWithThreeInnerJoins')
								->with('outputs',$data);	
		}
		
	}
	
////////////////////////////////////////////////////////////
				//json//
////////////////////////////////////////////////////////////
/**
* GET all - json
*/
	public function get_json#Object#s() {
		return Response::eloquent(#Object#::getAll());
	}
////////////////////////////////////////////////////////////
/**
* GET by id - json
*/
	public function get_json#Object#By($id) {
		//check id
		if ($id == '' || is_null($id) || $id <=0) {
		    return Response::json(array
							('error'=>'404',
							 'description' => Base_Controller::ConstMsgIdInvalid		
							),500);
		}
		
		//fetch the data from model
		$data = getModelRecordBy($id,'#Object#');		 
		if ($data == false) {
			return Redirect::to_route('#objects#');
			return Response::json(array
							('error'=>'404',
							 'description' => Base_Controller::ConstMsgModelEmpty		
							),500);
		}
		return Response::eloquent(#Object#::getBy($id));
	}
}//class

?>