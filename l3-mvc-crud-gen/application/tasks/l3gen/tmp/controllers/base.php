<?php
require('/../models/sharedModel.php');

class Base_Controller extends Controller {
	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */
	public function __call($method, $parameters)
	{
		return Response::error('404');
	}

////////////////////////////////////////////////////////////////////////
// Add shared controller stuff here 

/////////const
/**
* flash msg constants. error, fail, success for all crud
*/
const ConstMsgIdInvalid = 'Flash: Invalid id -Your request could not be processed. Please try again.';

const ConstMsgModelEmpty = 'Flash: Information not found. Please try again.';

const ConstMsgCreateNewFalse = 'could not be created. Sorry about that. Please try again later or contact us for help. Flash.'; 

const ConstMsgCreateNewTrue = 'added successfully! Flash.';

const ConstMsgUpdateFalse = 'could not be updated. Sorry about that. Please try again later or contact us for help. Flash.'; 

const ConstMsgUpdateTrue = 'updated successfully! Flash.'; 

const ConstMsgDeleteFalse = 'could not be removed. Sorry about that. Please try again later or contact us for help. Flash.'; 

const ConstMsgDeleteTrue = 'deleted successfully! Flash.'; 
}