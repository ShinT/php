<h1> {{$heading}}- #objects#/showAll.blade</h1>
{{-- var from $this->layout->mainContent ->with('heading','stuff') --}}	
	
<h5>List of #objects# from #object# model</h5>
<ol>

{{--10. CRUD - Read - List all #objects# - in index.blade view as links, then link each other to named route--> --}}

{{-- pass a 3rd param: array($#object#->id) b/c route '#object#/(:any)' '#objects#@view' requires #object# id to load data --}}

{{-- access output fields thru facade, so change in data store wont affect view. Just need to make changes in model --}}

@foreach($outputs as $output)
	<li>#Object# name: {{HTML::link_to_route('#object#', $output->#object#_#field1, array($output->id)) }} 
	</li>
{{--link to named route '#object#/(:any)', array('as'=> '#objects#',
	 'uses'=> '#objects#@view'))--}}
@endforeach	
</ol>	
	
{{-- link to add new #object# --}}
<p>{{HTML::link_to_route('new-#object#','Link to Add New #object# page') }} </p>