{{-- CRUD - Read - by id - create #objects#.view view file /views/#objects#/view.blade.php to view specific #object# by id --}}

<h1> {{ $heading }} - #objects#/show.blade</h1>

{{-- escape any output data displayed as html in views to prevent xss, stop script injection w/ HTML::entities or e --}}
<h2> #Object#'s Field1 - escaped by HTML::entities: {{HTML::entities($outputs->#object#_#field1)}} </h2>
<h2> #Object#'s Field1:- escaped by e: {{e($outputs->#object#_#field1)}} </h2>

<p>#Object#'s field2: {{$outputs->#object#_#field2 }}</p>

<p><small> Updated at: {{$outputs->#object#_updated_at }} </small> </p>


<span>
{{-- link to a list of #objects# page to go back to Home page --}}
	{{HTML::link_to_route('#objects#','Show all #objects#') }} <br/><br/>

{{-- Option to edit an #object# here n will lead to edit.blade for editing --}}
{{--Also pass a 3rd param: array($#object#->id) b/c route /(:any) requires #object# id to load data. $#object#-id goes to edit.blade to be stored as hidden field for submit event to update--}}

{{-- CRUD - edit #object#, get from #object#/$id/edit action --}}
	{{HTML::link_to_route('edit-#object#','Edit',array($outputs->id)) }} <br/><br/>

{{--  CRUD - delete #object#, form submit to #object#/delete action --}}
	{{Form::open('#object#/delete','DELETE',array('style'=>'display:inline;')) }}
	
	{{-- set csrf token for security check --}}
	{{Form::token()}}
	
{{-- set hidden field to store $id to send to submit for delete --}}
	{{Form::hidden('id', $outputs->id) }}
	
	{{Form::submit('Delete') }}

	{{Form::close() }}

{{-- set route --}}
</span>	

