<html>
	<h1>{{$heading}}</h1>

{{-- section to render any flash error msg after submit new --}}
{{render('shared.validationErrors') }}

{{-- use form class to build forms --}}
{{Form::open('#objects#/create','POST') }}

{{-- csrf security check --}}
{{Form::token()}}

<p>

{{Form::label('#field1','#field1:') }} <br/>
{{Form::text('#object#_#field1',Input::old('#object#_#field1')) }} <br/>
{{-- Specify Input::old('dbField')) so no need to retype after submit w/ validate err --}}
	
</p>

<p>
{{Form::label('#field2','#Field2:') }} <br/>
{{Form::textarea('#object#_#field2', Input::old('#object#_#field2')) }} <br/>
{{-- Specify Input::old('dbField')) so no need to retype after submit w/ validate err --}}
</p>

<p> {{Form::submit('Add #object#') }} </p>

{{Form::close() }}

</html>

