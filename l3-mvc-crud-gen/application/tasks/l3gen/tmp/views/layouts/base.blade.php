<!DOCTYPE HTML>
<html>
<head>  
	<title>Base layout | {{$titleContent}}</title>
</head>

<body>
	{{-- show any flash msg after post processing --}}
	@if(Session::has('flashMsg'))
		<p style= "color:blue">{{Session::get('flashMsg')}} </p>
	@endif 
	
	{{$mainContent}}
</body>
</html>