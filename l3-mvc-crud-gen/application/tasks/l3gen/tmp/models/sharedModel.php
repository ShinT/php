<?php
/**
*shared model helper functions for Eloquent ORM and Fluent. Add require_once('/../models/sharedModel.php') in /controllers/base.php
* 
* Assumes the convention:
* - model name:singular, db table name: plural
* - $fields array - Abstract data store fields from view w/ facade in each model
*   $fields format: objectNameSingular_fieldName. Construct on the fly in each func ; 
*/	

//////////////////////////////////////////////////////////
/**
* required const. Specify whether to use eloquent or fluent for data processing
* To use eloquent, need to 1st define class ModelName extends Eloquent in /models
* @param: Eloquent | Fluent
* 
*/
	const ConstDataProcessTypeEloquent = 'Eloquent';
	const ConstDataProcessTypeFluent = 'Fluent';	

////////////////////////////////////////////////////////////
/**
* returns Validation object after input is validated against rules defined in each controller
* @param array $inputData
* @param array $rules
* @return ValidationObject
*/
	function getValidationResult($inputData,$rules) {
		return Validator::make($inputData,$rules);
	}	

////////////////////////////////////////////////////////////
//CRUD - Read 1a
/**
* returns a collection of data objects from model. Cast return to array for binding. Specify table name for ConstDataProcessTypeFluent
* @param string $modelName
* @param string $sortFieldBy
* @param string asc|desc $sortDirection
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param array $fields
* @param string optional $tableName
* @return Collection|false
*/
	function getModelRecords($modelName,$sortFieldBy,$sortDirection,$dataProcessType
							,$fields ,$tableName ='') {
		//for testing
		//return false;
		
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			return $modelName::order_by($sortFieldBy,$sortDirection)->get($fields);	
		}
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if($tableName == '') {
				return false;
			}
			return DB::table($tableName)->order_by($sortFieldBy,$sortDirection)
										->get($fields);
		}
			
	}
	
////////////////////////////////////////////////////////////
//CRUD - Read 1b
/**
* returns a collection of data objects from model with limit options. Cast return to array for binding
* @param int $limit
* @param string $modelName
* @param string $sortFieldBy
* @param string asc|desc $sortDirection
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param array $fields
* @param string optional $tableName
* @return Collection|false
*/
	function getModelWithLimit($limit,$modelName,$sortFieldBy,$sortDirection,
							$dataProcessType,$fields, $tableName ='') {
		
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			return $modelName::take($limit)->order_by($sortFieldBy,$sortDirection)
			->get();	
		}
		
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if($tableName == '') {
				return false;
			}
			return DB::table($tableName)
			->select($fields)
			->take($limit)->order_by($sortFieldBy,$sortDirection)
			->get();
		}
		
	}

////////////////////////
//CRUD - Read 2
/**
* returns a data object record by id. Cast return to array for binding. 
* @param int $id
* @param string $modelName
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param array $fields
* @param string optional $tableName 
* @return false|Object
*/
	function getModelRecordBy($id,$modelName,$dataProcessType,$fields,$tableName ='') {
		//for testing
		 //return false;
		
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			return $modelName::find($id,$fields);	
		}
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if($tableName == '') {
				return false;
			}
			return DB::table($tableName)->find($id,$fields);
		}
		
	}

////////////////////////////////////////////////////////////
//CRUD - Read 2 joins
/**
* @param string $id
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param array $selectFields
* @param string $mainTableName
* @param string $tableName2
* @param string $joinField1
* @param string $joinOperator
* @param string $joinField2
* @param string $whereField1
* @param string $whereOperator
* @param string $whereField2
* @param string $sortFieldBy
* @param string asc|desc $sortDirection
* @return array|false
*/
	function getModelRecordWithTwoInnerJoins($id,$dataProcessType,$selectFields
	,$mainTableName,$tableName2,$joinField1,$joinOperator,$joinField2
	,$whereField1,$whereOperator, $whereField2, $sortFieldBy,$sortDirection) 
	{
	
	//for testing
	//return false;
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			// use fluent for joins for now
			return false;
		}	
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if($mainTableName == '') {
				return false;
			}
			
		//bug: column alias could only use small case w/ a _, no camel case or dot
		return DB::table($mainTableName)
			->select($selectFields)
			->join($tableName2,$joinField1,$joinOperator,$joinField2) 
			->where($whereField1,$whereOperator,$whereField2)
			->order_by($sortFieldBy,$sortDirection)
			->get();
		
		//i.e 
		/* DB::table($mainTableName)
		->select(array('category_id','title','author_id','name As author_name'))
		->join($tableName2, 'authors.id','=','articles.author_id')
		->where('authors.id','=',$id)
		->order_by('articles.title','asc')
		*/
		}//process fluent query
	}

////////////////////////////////////////////////////////////
//CRUD - Read - 3 joins
/**
* @param string $id
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param array  $selectFields
* @param string $mainTableName
* @param string $tableName2
* @param string $joinField1
* @param string $joinOperator1 
* @param string $joinField2
* @param string $tableName3
* @param string $joinField3
* @param string $joinOperator2
* @param string $joinField4
* @param string $whereField1
* @param string $whereOperator1
* @param string $whereField2
* @param string $sortFieldBy
* @param string asc|desc $sortDirection
* @return array|false
*/
	function getModelRecordWithThreeInnerJoins
	($id,$dataProcessType,$selectFields,$mainTableName,$tableName2,$joinField1, 
	$joinOperator1,$joinField2,$tableName3,$joinField3,$joinOperator2,$joinField4, 		$whereField1,$whereOperator1,$whereField2, $sortFieldBy, $sortDirection) 
	{
											
		//for testing
		//return false;									
		
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			return false; // not supported at this time
		}
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if ($mainTableName == '' || $tableName2 == '' || $tableName3 == '') {
				return false;
			}
			
			return DB::table($mainTableName)
					->select($selectFields)
					->join($tableName2,$joinField1,$joinOperator1,$joinField2)
					->join($tableName3,$joinField3,$joinOperator2,$joinField4)
					->where($whereField1,$whereOperator1,$whereField2)
					->order_by($sortFieldBy,$sortDirection)
					->get();
					
					/*->select('articles.title As title')
					->join('articles','authors.id','=','articles.author_id')
					->join('categories','categories.id','=','articles.category_id')
					->where('authors.id','=',$id)
					->order_by('articles.title','asc')
					->get();*/
		}									
	}	
////////////////////////////////////////////////////////////
//CRUD - Create
/**	
* create a new data object record with an array of input data, mapped to fields
* @param string $modelName
* @param array $mappedInputData
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param string optional $tableName  
* @return false|Object|int
*/	
	function setModelNewRecord($modelName,$mappedInputData,
	$dataProcessType,$tableName ='') {
	//for testing
	//return false;
	//return 0;
	
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			return $modelName::create($mappedInputData);	
		}
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if($tableName == '') {
				return 0;
			}
			return DB::table($tableName)->insert_get_id($mappedInputData);
		}
	
	}

////////////////////////////////////////////////////////////
//CRUD - Update
/**
* update a data object record with html form input data. Success = 1, fail = 0
* @param int $id
* @param string $modelName
* @param array $inputData
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessType
* @param string optional $tableName 
* @return int
*/
	function setModelUpdateRecord($id,$modelName,$inputData,$dataProcessType
								,$tableName =''){
	//for testing
	//$id = -1;
	
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			return $modelName::update($id,$inputData);		
		}
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			if($tableName == '') {
				return 0;
			}
			return DB::table($tableName)
					->where('id','=',$id)
					->update($inputData);
		}
	}

////////////////////////
//CRUD delete	
/**
* delete a data object record 
* @param int $id
* @param string $modelName
* @param ConstDataProcessTypeEloquent | ConstDataProcessTypeFluent $dataProcessingType
* @param string optional $tableName  
* @return int
*/
	function setModelDeleteRecord($id,$modelName,$dataProcessType,$tableName ='') {
		//for testing
		//$id = -1;
	
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			$foundRecord = $modelName::find($id);
				if ((bool)$foundRecord) {
				//proceed to delete	
				return $foundRecord->delete();
			
				}// array = true after bool cast = not empty
				else {
		 				return 0;
				}	
		}	
	
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
					if ($tableName == '') {
						return 0;
					}
					else {
						return DB::table($tableName)->delete($id);
					}
		
				}
		
		return 0;
	}
?>