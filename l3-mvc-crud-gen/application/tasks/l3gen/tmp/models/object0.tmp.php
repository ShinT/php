<?php
class #Object# extends Eloquent{

	//hide attribute in the model
	//public static $hidden = array('fieldToHide');

	//auto update timestamps. Default is true.
	//public static $timestamps = true;

	// updatable fields. Guard against mass update. 
	public static $accessible = array('#field1','#field2');
 	
	//$json support
	public static $json = false;
	
	// prep params
	private static $tableName = '#objects#';
		
	//data rules
	public static $rules = array(
	'#field_1' => 'required|min:5',
	'#field_2' => 'required|min:10'
	
	);
	//data rules

////////////////////////////////////////////////////////////
// CRUD - Read -	
	public static function getAll() {
		//note: abstract fields from view w/ alias facade: objectNameSingular_fieldName 	
		$fields = array(
			'id',
			'#field1 As #object#_field1',
			'#field2 As #object#_field2'
		);
	
		$dataProcessType; 
		if (self::$json == true) {
			$dataProcessType = ConstDataProcessTypeEloquent;
		}
		else {
		$dataProcessType = ConstDataProcessTypeFluent;
		}
		
		return getModelRecords('#Object#','#field','asc'
		,$dataProcessType,$fields,self::$tableName);
	}

////////////////////////////////////////////////////////////
// CRUD - Read - with limit	
	public static function getAllWithLimit($limit) {
		//note: abstract fields from view w/ alias facade: objectNameSingular_fieldName 	
		$fields = array(
			'id',
			'#field1 As #object#_field1',
			'#field2 As #object#_field2'
		);
		
		$dataProcessType; 
		if (self::$json == true) {
			$dataProcessType = ConstDataProcessTypeEloquent;
		}
		else {
			$dataProcessType = ConstDataProcessTypeFluent;
		}	
		
		return getModelWithLimit($limit,'#Object#','#field','desc'
		,$dataProcessType,$fields,self::$tableName);
	}
			
///////////////////////////////////////////////	
//CRUD - Read - by id
	public static function getBy($id) {
		$fields = array(
			'id',
			'#field1 As #object#_field1',
			'#field2 As #object#_field2',
			'updated_at As #object#_updated_at'
		);
		
		$dataProcessType; 
		if (self::$json == true) {
			$dataProcessType = ConstDataProcessTypeEloquent;
		}
		else {
			$dataProcessType = ConstDataProcessTypeFluent;
		}	
			
		return getModelRecordBy($id,'#Object#',$dataProcessType,
							$fields,self::$tableName);
	}

////////////////////////////////////////////////////////////
//CRUD - Create - new
	public static function postNew($inputData) {
		$mappedInputData;
		$dataProcessType = ConstDataProcessTypeFluent;
		
		// map facade input data to actual model fields 
		//Need to update timestamp manually for fluent and raw
			if ($dataProcessType == ConstDataProcessTypeEloquent){
				$mappedInputData = array('#field1'=>$inputData['#object#_field1'],
						 				'#field2'=>$inputData['#object#_field2']
										);	
			}
			else if ($dataProcessType == ConstDataProcessTypeFluent) {
				$mappedInputData = array('#field1' => $inputData['#object#_field1'],
								'#field2' => $inputData['#object#_field2'],
								'created_at' => new \Datetime,
								'updated_at' => new \Datetime 
								);	
			}
		
		return setModelNewRecord('#Object#',$mappedInputData,
		$dataProcessType,self::$tableName);
	}

////////////////////////////////////////////////////////////
	//CRUD Put - update
	public static function putUpdate($id,$inputData) {
		$mappedInputData;
		$dataProcessType = ConstDataProcessTypeEloquent;
		
		//map facade input data to actual model fields
		if ($dataProcessType == ConstDataProcessTypeEloquent) {
			$mappedInputData = array(
			'#field1'=> $inputData['#object#_field1'],
			'#field2'=> $inputData['#object#_field2']
			);
		}
		//Need to update timestamp manually for fluent and raw
		else if ($dataProcessType == ConstDataProcessTypeFluent) {
			$mappedInputData = array(
			'#field1'=> $inputData['#object#_field1'],
			'#field2'=> $inputData['#object#_field2'],
			'updated_at' => new \Datetime
			);
		}
		
		//process 
		return setModelUpdateRecord($id,'#Object#',$mappedInputData,$dataProcessType,'#objects#');
		
	}
	
////////////////////////////////////////////////////////////
	//CRUD - delete
	public static function deleteRemove($id) {
		return setModelDeleteRecord($id,'#Object#',ConstDataProcessTypeFluent,'#objects#');
	}

////////////////////////////////////////////////////////////
////////2 joins
	public static function getTwoInnerJoins($id) {
		$fields = array(
				'articles.category_id As article_category_id',
				'articles.title As article_title',
				'articles.author_id As article_author_id',
				'authors.name As author_name'	
		);
		
		$dataProcessType; 
		if (self::$json == true) {
			$dataProcessType = ConstDataProcessTypeEloquent;
		}
		else {
			$dataProcessType = ConstDataProcessTypeFluent;
		}	
		  
		return getModelRecordWithTwoInnerJoins
		($id,$dataProcessType,$fields,'authors','articles','authors.id','=',
		'articles.author_id','authors.id','=',$id,'article_title','asc'
		);
	}


////////3 joins
	public static function getThreeInnerJoins($id) {
		$fields = array(
			'authors.id As author_id',
			'authors.name As author_name',
			'articles.id As article_id',
			'articles.title As article_title',
			'categories.id As category_id',
			'categories.name As category_name'	
		);
		
		$dataProcessType; 
		if (self::$json == true) {
			$dataProcessType = ConstDataProcessTypeEloquent;
		}
		else {
			$dataProcessType = ConstDataProcessTypeFluent;
		}	
		
		return getModelRecordWithThreeInnerJoins
				($id,$dataProcessType,$fields,'authors','articles','authors.id',
				'=','articles.author_id','categories','articles.category_id','='
				,'categories.id','authors.id','=',$id,'articles.title','desc'							);
												
	
	}


}//class


?>