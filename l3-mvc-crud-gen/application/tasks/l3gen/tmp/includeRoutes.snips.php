////////////////////////////////////////////////////////////////////
/**
* #Objects# controller routes
*/

// CRUD - read - all - GET
//get(pathToUrl,array('uses' =>'controller@MethodToRun'))
// 'as' = named route
Route::get('#objects#',array('as'=> '#objects#', 'uses' =>'#objects#@all'));

// CRUD - read - with limit - GET
Route::get('#objects#withlimit', array('as' =>'#objects#-limit','uses'=> '#objects#@allWithLimit'));

// CRUD - read by $id - also register named route 'as' => 'namedRoute'
Route::get('#object#/(:any)', array('as'=> '#object#', 'uses'=> '#objects#@by'));

// CRUD - create new - GET 
Route::get('#objects#/new', array('as'=> 'new-#object#', 'uses'=> '#objects#@new'));

// CRUD - create new - POST 
// no need named route b/c it's only accepting input to process stuff after being posted
// and no link creation or redirected to this
// csrf security check. Add csrf before filter forgery check. If forged, return 500
Route::post('#objects#/create', array('before'=> 'csrf',  'uses' => '#objects#@new'));	

// CRUD - read - edit - GET
//note: 'any', need to include $id in return Redirect::to_route('edit-#object#', $id) in controller
Route::get('#object#/(:any)/edit',array('as'=> 'edit-#object#', 'uses'=> '#objects#@edit'));

// CRUD - update - PUT
// no need named route b/c it's only accepting input to process stuff after being submitted and   no link creation or redirected to this
// csrf security check. Add csrf before filter forgery check. If forged, return 500
Route::put('#object#/update', array('before'=> 'csrf', 'uses' => '#objects#@update'));

// CRUD - delete
//no need named route b/c it's only accepting input to process stuff after being posted
Route::delete('#object#/delete',array('before'=>'csrf','uses'=>'#objects#@remove'));

//joins GET
Route::get('test3innerjoins',array('as'=>'test3innerjoins','uses'=>'#objects#@test3innerjoins'));
Route::get('test2innerjoins',array('as'=>'test2innerjoins','uses'=>'#objects#@test2innerjoins'));

//json GET
Route::get('json-#objects#', array('as'=> 'json#Objects#', 'uses'=>'#objects#@json#Objects#'));
Route::get('json-#object#/(:any)', array('as'=> 'json-#object#', 'uses'=> '#objects#@json#Object#By'));

////////////////////////////////////////////////////////////////////
