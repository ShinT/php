
////////////////////////////////////////////////////////////////////
/**
* Monies controller routes
*/

// CRUD - read - all - GET
//get(pathToUrl,array('uses' =>'controller@MethodToRun'))
// 'as' = named route
Route::get('monies',array('as'=> 'monies', 'uses' =>'monies@all'));

// CRUD - read - with limit - GET
Route::get('monieswithlimit', array('as' =>'monies-limit','uses'=> 'monies@allWithLimit'));

// CRUD - read by $id - also register named route 'as' => 'namedRoute'
Route::get('money/(:any)', array('as'=> 'money', 'uses'=> 'monies@by'));

// CRUD - create new - GET 
Route::get('monies/new', array('as'=> 'new-money', 'uses'=> 'monies@new'));

// CRUD - create new - POST 
// no need named route b/c it's only accepting input to process stuff after being posted
// and no link creation or redirected to this
// csrf security check. Add csrf before filter forgery check. If forged, return 500
Route::post('monies/create', array('before'=> 'csrf',  'uses' => 'monies@new'));	

// CRUD - read - edit - GET
//note: 'any', need to include $id in return Redirect::to_route('edit-money', $id) in controller
Route::get('money/(:any)/edit',array('as'=> 'edit-money', 'uses'=> 'monies@edit'));

// CRUD - update - PUT
// no need named route b/c it's only accepting input to process stuff after being submitted and   no link creation or redirected to this
// csrf security check. Add csrf before filter forgery check. If forged, return 500
Route::put('money/update', array('before'=> 'csrf', 'uses' => 'monies@update'));

// CRUD - delete
//no need named route b/c it's only accepting input to process stuff after being posted
Route::delete('money/delete',array('before'=>'csrf','uses'=>'monies@remove'));

//joins GET
Route::get('test3innerjoins',array('as'=>'test3innerjoins','uses'=>'monies@test3innerjoins'));
Route::get('test2innerjoins',array('as'=>'test2innerjoins','uses'=>'monies@test2innerjoins'));

//json GET
Route::get('json-monies', array('as'=> 'jsonMonies', 'uses'=>'monies@jsonMonies'));
Route::get('json-money/(:any)', array('as'=> 'json-money', 'uses'=> 'monies@jsonMoneyBy'));

////////////////////////////////////////////////////////////////////
