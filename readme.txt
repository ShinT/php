//
//  l3-mvc-crud-gen
//
//  Created by Shin Tan on 4/7/13.
//  Copyright (c) 2013 PimpFit.com All rights reserved.
//

/*
 * l3-mvc-crud-gen is an MVC-based CRUD generator that speeds up the creation of CRUD applications in PHP based on the open-source Laravel PHP framework.

 * With just 1 or 2 clicks (or using a command line batch file), you can auto-generate a working, enterprise-level CRUD starter project from scratch. 


* Eliminates 50% of the plumbing CRUD procedures upfront. Saves you lots of time, so you could focus on your business requirements. 

*/